package org.efreak.chadmin;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.efreak.chadmin.channel.Channel;


public class ChatPlayer implements OfflinePlayer {

	private OfflinePlayer player;
	private Channel channel = null;
	
	public ChatPlayer(OfflinePlayer arg1Player) {
		player = arg1Player;
	}
	
	public ChatPlayer(OfflinePlayer arg1Player, Channel arg2Channel) {
		player = arg1Player;
		setChannel(arg2Channel);
	}

	@Override
	public boolean isOp() {
		return player.isOp();
	}

	@Override
	public void setOp(boolean arg0) {
		player.setOp(arg0);
	}

	@Override
	public Map<String, Object> serialize() {
		return player.serialize();
	}

	@Override
	public Location getBedSpawnLocation() {
		return player.getBedSpawnLocation();
	}

	@Override
	public long getFirstPlayed() {
		return player.getFirstPlayed();
	}

	@Override
	public long getLastPlayed() {
		return player.getLastPlayed();
	}

	@Override
	public String getName() {
		return player.getName();
	}

	@Override
	public Player getPlayer() {
		return player.getPlayer();
	}

	@Override
	public boolean hasPlayedBefore() {
		return player.hasPlayedBefore();
	}

	@Override
	public boolean isBanned() {
		return player.isBanned();
	}

	@Override
	public boolean isOnline() {
		return player.isOnline();
	}

	@Override
	public boolean isWhitelisted() {
		return player.isWhitelisted();
	}

	@Override
	public void setBanned(boolean arg0) {
		player.setBanned(arg0);
	}

	@Override
	public void setWhitelisted(boolean arg0) {
		player.setWhitelisted(arg0);
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	public void setChannel(Channel arg1Channel) {
		if (channel != null) channel.removePlayer(this);
		channel = arg1Channel;
		channel.addPlayer(this);
	}
	
	public OfflinePlayer getOfflinePlayer() {
		return player;
	}
}
