package org.efreak.chadmin;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.plugin.Plugin;

/**
 *
 * The IOManager Handles all Input and Output
 *
 * @author efreak1996
 *
 */
public class IOManager {

    private static Configuration config;
    public static Plugin plugin;
    private static ConversationFactory conversationFactory;
    private static HashMap<String, Conversation> conversations;
    public static String prefix = ChatColor.DARK_RED + "[Chadmin] " + ChatColor.WHITE;
    public static String error = ChatColor.RED + "[Error] " + ChatColor.WHITE;
    public static String warning = ChatColor.YELLOW + "[Warning] " + ChatColor.WHITE;
    private static Translator translator;
    private static boolean color = true;

    public void initialize() {
        config = new Configuration();
        color = config.getBoolean("IO.ColoredLogs");
        conversationFactory = new ConversationFactory(plugin);
        prefix = color(config.getString("IO.Prefix")) + " " + ChatColor.WHITE;
        error = color(config.getString("IO.Error")) + " " + ChatColor.WHITE;
        warning = color(config.getString("IO.Warning")) + " " + ChatColor.WHITE;
        translator = new Translator();
        translator.initialize();
    }

    public void broadcast(String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            Bukkit.getServer().broadcastMessage(color(IOManager.prefix + msg));
        } else {
            Bukkit.getServer().broadcastMessage(color(msg));
        }
    }

    public void broadcast(Boolean prefix, String msg) {
        if (prefix) {
            Bukkit.getServer().broadcastMessage(color(IOManager.prefix + msg));
        } else {
            Bukkit.getServer().broadcastMessage(color(msg));
        }
    }

    public void broadcast(String msg, String perm) {
        if (config.getBoolean("IO.Show-Prefix")) {
            Bukkit.getServer().broadcast(color(IOManager.prefix + msg), perm);
        } else {
            Bukkit.getServer().broadcast(color(msg), perm);
        }
    }

    public void broadcast(Boolean prefix, String msg, String perm) {
        if (prefix) {
            Bukkit.getServer().broadcast(color(IOManager.prefix + msg), perm);
        } else {
            Bukkit.getServer().broadcast(color(msg), perm);
        }
    }

    public void sendConsole(String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(msg));
        }
    }

    public void sendConsole(String msg, boolean prefix) {
        if (prefix) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(msg));
        }
    }

    public void sendConsoleWarning(String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + IOManager.warning + ChatColor.YELLOW + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.warning + msg));
        }
    }

    public void sendConsoleWarning(String msg, boolean prefix) {
        if (prefix) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + IOManager.warning + ChatColor.YELLOW + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.warning + msg));
        }
    }

    public void sendConsoleError(String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + IOManager.error + ChatColor.RED + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.error + msg));
        }
    }

    public void sendConsoleError(String msg, boolean prefix) {
        if (prefix) {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.prefix + IOManager.error + ChatColor.RED + msg));
        } else {
            plugin.getServer().getConsoleSender().sendMessage(color(IOManager.error + msg));
        }
    }

    public void send(CommandSender sender, String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + msg));
        } else {
            sender.sendMessage(color(msg));
        }
    }

    public void send(CommandSender sender, String msg, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + msg));
        } else {
            sender.sendMessage(color(msg));
        }
    }

    public void sendTranslation(CommandSender sender, String key) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + translate(key)));
        } else {
            sender.sendMessage(color(translate(key)));
        }
    }

    public void sendTranslation(CommandSender sender, String key, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + translate(key)));
        } else {
            sender.sendMessage(translate(key));
        }
    }

    public void sendWarning(CommandSender sender, String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + IOManager.warning + ChatColor.YELLOW + msg));
        } else {
            sender.sendMessage(color(IOManager.warning + msg));
        }
    }

    public void sendWarning(CommandSender sender, String msg, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + IOManager.warning + ChatColor.YELLOW + msg));
        } else {
            sender.sendMessage(color(IOManager.warning + msg));
        }
    }

    public void sendError(CommandSender sender, String msg) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + IOManager.error + ChatColor.RED + msg));
        } else {
            sender.sendMessage(color(IOManager.error + msg));
        }
    }

    public void sendError(CommandSender sender, String msg, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + IOManager.error + ChatColor.RED + msg));
        } else {
            sender.sendMessage(color(IOManager.warning + msg));
        }
    }

    public void sendFewArgs(CommandSender sender, String usage) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + translate("Command.FewArgs")));
            sender.sendMessage(color(IOManager.prefix + translate("Command.Usage").replaceAll("%usage%", usage)));
        } else {
            sender.sendMessage(translate("Command.FewArgs"));
            sender.sendMessage(translate("Command.Usage").replaceAll("%usage%", usage));
        }
    }

    public void sendFewArgs(CommandSender sender, String usage, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + translate("Command.FewArgs")));
            sender.sendMessage(color(IOManager.prefix + translate("Command.Usage").replaceAll("%usage%", usage)));
        } else {
            sender.sendMessage(translate("Command.FewArgs"));
            sender.sendMessage(translate("Command.Usage").replaceAll("%usage%", usage));
        }
    }

    public void sendManyArgs(CommandSender sender, String usage) {
        if (config.getBoolean("IO.Show-Prefix")) {
            sender.sendMessage(color(IOManager.prefix + translate("Command.ManyArgs")));
            sender.sendMessage(color(IOManager.prefix + translate("Command.Usage").replaceAll("%usage%", usage)));
        } else {
            sender.sendMessage(translate("Command.ManyArgs"));
            sender.sendMessage(translate("Command.Usage").replaceAll("%usage%", usage));
        }
    }

    public void sendManyArgs(CommandSender sender, String usage, boolean prefix) {
        if (prefix) {
            sender.sendMessage(color(IOManager.prefix + translate("Command.ManyArgs")));
            sender.sendMessage(color(IOManager.prefix + translate("Command.Usage").replaceAll("%usage%", usage)));
        } else {
            sender.sendMessage(translate("Command.ManyArgs"));
            sender.sendMessage(translate("Command.Usage").replaceAll("%usage%", usage));
        }
    }

    public void createConversation(CommandSender sender, String name, Prompt prompt) {
        conversationFactory.withFirstPrompt(prompt);
        conversations.put(name, conversationFactory.buildConversation((Conversable) sender));
    }

    public Conversation getConversation(String name) {
        return conversations.get(name);
    }

    public String translate(String key) {
        return color(translator.getKey(key));
    }

    public Translator getTranslator() {
        return translator;
    }

    public String color(String msg) {
        if (color) {
            return parseColor(msg);
        } else {
            return remColor(msg);
        }
    }

    public String parseColor(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public String remColor(String message) {
        return ChatColor.stripColor(message);
    }
}
