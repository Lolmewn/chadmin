package org.efreak.chadmin;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import org.bukkit.Bukkit;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import org.efreak.chadmin.Database;
import org.efreak.chadmin.databases.*;
import org.efreak.chadmin.Updater.UpdateResult;
import org.efreak.chadmin.Updater.UpdateType;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.channel.ChannelAvailability;
import org.efreak.chadmin.channel.ChannelMode;
import org.efreak.chadmin.chatvariables.*;
import org.efreak.chadmin.commands.CmCommandExecutor;
import org.efreak.chadmin.help.HelpManager;

public class Chadmin extends JavaPlugin {
    
    private static Plugin instance;
    private static Configuration config;
    private static IOManager io;
    private static Permissions permHandler;
    private static HelpManager helpManager;
    private static ChatVariableManager varManager;
    private static Database db;
    private static File homeDir;
    private static List<Channel> channelList;
    private static HashMap<String, Channel> channelMap;
    private static HashMap<String, ChatPlayer> players;
    
    @Override
    public void onLoad() {
        instance = this;
    }
    
    @Override
    public void onEnable() {
        homeDir = getDataFolder();
        new File(homeDir, "lang").mkdirs();
        config = new Configuration();
        config.initalize();
        io = new IOManager();
        io.initialize();
        Database.registerDatabaseSystem("MySQL", new MySQL());
        Database.registerDatabaseSystem("SQLite", new SQLite());
        db = Database.getDatabaseBySystem(config.getString("Database.System"));
        if (db == null) {
            io.sendConsoleWarning("Unknown Database System. Falling back to SQLite");
            db = Database.getDatabaseBySystem("SQLite");
        }
        db.init();
        permHandler = new Permissions();
        permHandler.initialize();
        io.sendConsole(io.translate("Channels.Loading"));
        channelList = new ArrayList<Channel>();
        channelMap = new HashMap<String, Channel>();
        varManager = new ChatVariableManager();
        varManager.registerHandler(PrefixVariables.class);
        varManager.registerHandler(SuffixVariables.class);
        varManager.registerHandler(EconomyVariables.class);
        varManager.registerHandler(PlayerVariables.class);
        varManager.registerHandler(EnvironmentVariables.class);
        if (new File(homeDir, "channels").mkdirs()) {
            new Channel("Default", ChannelMode.PUBLIC, ChannelAvailability.GLOBAL);
            //new Channel("Local", ChannelMode.PUBLIC, ChannelAvailability.LOCAL, 100);
            //new Channel("Admin", ChannelMode.PRIVATE, ChannelAvailability.GLOBAL, new ChannelPassword("123456"));
        }
        File[] channelFiles = new File(homeDir, "channels").listFiles(new ymlFilter());
        for (int i = 0; i < channelFiles.length; i++) {
            Channel channel = new Channel(channelFiles[i].getName().replaceAll("_", " ").replaceAll(".yml", ""));
            registerChannel(channel);
        }
        helpManager = new HelpManager();
        helpManager.initialize();
        getCommand("cm").setExecutor(new CmCommandExecutor());
        players = new HashMap<String, ChatPlayer>();
        getServer().getPluginManager().registerEvents(new ChatListener(), this);
        try {
            new Metrics(this).start();
        } catch (IOException ex) {
            Logger.getLogger(Chadmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        io.sendConsole(io.translate("Plugin.Done"));
        if (config.getBoolean("General.Auto-Updater")) {
            io.sendConsole(io.translate("AutoUpdater.CheckingUpdates"));
            Updater u = new Updater(this, 43051, this.getFile(), UpdateType.DEFAULT, false);
            if (u.getResult().equals(UpdateResult.SUCCESS)) {
                io.sendConsole(io.translate("AutoUpdater.NewVersion").replaceAll("%name%", u.getLatestName()));
                io.sendConsole(io.translate("AutoUpdater.Running").replaceAll("%name%", "Chadmin " + getDescription().getVersion()));
            } else {
                io.sendConsole(io.translate("AutoUpdater.UpToDate"));
            }
        }
    }
    
    @Override
    public void onDisable() {
        io.sendConsole(io.translate("Channels.Saving"));
        for (int i = 0; i < channelList.size(); i++) {
            channelList.get(i).save();
        }
        io.sendConsole(io.translate("Plugin.Done"));
    }
    
    public static String getFormat(ChatPlayer player) {
        Channel channel = player.getChannel();
        String msg = varManager.parseVariables(channel.getFormat(), player);
        if (config.getBoolean("Format.Colorcodes")) {
            msg = io.parseColor(msg);
        }
        return msg;
    }
    
    public static Channel getDefaultChannel() {
        return channelMap.get(config.getString("Channel.Default"));
    }
    
    public static Channel getDefaultChannel(World world) {
        return null;
    }
    
    public static Channel getChannel(String name) {
        if(channelMap.containsKey(name)){
            return channelMap.get(name);
        }
        for(String key : channelMap.keySet()){
            if(key.toLowerCase().startsWith(name.toLowerCase())){
                return channelMap.get(key);
            }
        }
        return null;
    }
    
    public static boolean registerChannel(Channel arg1Channel) {
        if (channelList.contains(arg1Channel)) {
            return false;
        } else {
            io.sendConsole(io.translate("Channel.Found").replaceAll("%channel%", arg1Channel.getName()));
            channelList.add(arg1Channel);
            channelMap.put(arg1Channel.getName(), arg1Channel);
            return true;
        }
        
    }
    
    public static List<Channel> getChannels() {
        return channelList;
    }
    
    public static Plugin getInstance() {
        return instance;
    }
    
    public static IOManager getIOManager() {
        return io;
    }
    
    public static Configuration getConfiguration() {
        return config;
    }
    
    public static Database getDb() {
        return db;
    }
    
    public static ChatPlayer getChatPlayer(String name) {
        if (players.containsKey(name)) {
            return players.get(name);
        }
        ChatPlayer player;
        Player p = Bukkit.getPlayer(name);
        if(p == null){
            player = new ChatPlayer(Bukkit.getServer().getOfflinePlayer(name));
        }else{
            player = new ChatPlayer(p);
        }
        Chadmin.addChatPlayer(player);
        player.getChannel().goOnline(player);
        return players.get(name);
    }
    
    public static ChatPlayer getChatPlayer(Player player) {
        return players.get(player.getName());
    }
    
    public static void addChatPlayer(ChatPlayer player) {
        Channel channel = null;
        for (int i = 0; i < channelList.size(); i++) {
            if (channelList.get(i).getPlayers().contains(player.getOfflinePlayer())) {
                channel = channelList.get(i);
            }
        }
        if (channel == null) {
            player.setChannel(getDefaultChannel());
        } else {
            player.setChannel(channel);
        }
        players.put(player.getName(), player);
    }
    
    public static void removeChatPlayer(String name) {
        players.remove(name);
    }
    
    public static void removeChannel(Channel channel) {
        channelList.remove(channel);
        channelMap.remove(channel.getName());
        channel.remove();
    }
}

class ymlFilter implements FilenameFilter {
    
    public boolean accept(File dir, String name) {
        return (name.endsWith(".yml"));
    }
}
