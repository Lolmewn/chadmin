package org.efreak.chadmin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

/**
 *
 * Loads and manages the config File
 *
 * @author efreak1996
 *
 */
public class Configuration {

    private static IOManager io;
    private static Plugin plugin;
    private static FileConfiguration config;
    private static File configFile;
    private static String dbType = "SQLite";

    /**
     *
     * Loads and creates if needed the config
     *
     */
    public void initalize() {
        plugin = Chadmin.getInstance();
        configFile = new File(plugin.getDataFolder() + File.separator + "config.yml");
        io = new IOManager();
        io.plugin = plugin;
        config = plugin.getConfig();
        if (!(configFile.exists())) {
            io.sendConsole("Creating config.yml...", true);
            try {
                configFile.createNewFile();
                UpdateConfig();
                io.sendConsole("config.yml succesfully created!", true);
                config.load(configFile);
            } catch (IOException e) {
                if (getDebug()) {
                    e.printStackTrace();
                }
            } catch (InvalidConfigurationException e) {
                if (getDebug()) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                config.load(configFile);
                UpdateConfig();
                config.load(configFile);
            } catch (IOException e) {
                if (getDebug()) {
                    e.printStackTrace();
                }
            } catch (InvalidConfigurationException e) {
                if (getDebug()) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *
     * Updates a already existing config
     *
     * @throws IOException
     *
     */
    public void UpdateConfig() throws IOException {
        update("General.Use-Permissions", true);
        update("General.Use-Vault", true);
        update("General.Auto-Updater", true);
        update("General.Debug", false);
        update("General.Statistics.Enabled", true);
        update("General.Statistics.GUID", UUID.randomUUID().toString());
        addDefault("General.Statistics.GUID", getString("General.Statistics.GUID"));
        update("Format.Message", "%prefix%%name%%suffix%: &f%msg%");
        update("Format.Colorcodes", true);
        update("Channel.List.Entry", "%mode% %availability% %name% %players%");
        update("Channel.Default", "Default");
        update("Channel.notifyMuted", true);
        update("Highlighter.Enabled", true);
        update("Highlighter.Format", "&l&n&7%player%&r");
        //update("Highlighter.Learn", true);
        update("IO.Show-Prefix", true);
        update("IO.Prefix", "&4[Chadmin]");
        update("IO.Error", "&c[Error]");
        update("IO.Warning", "&e[Warning]");
        update("IO.Language", "en");
        update("IO.ColoredLogs", true);
        update("IO.HelpFormat", "&e%cmd% %args%: &f%desc%");
        update("Database.System", "SQLite");
        /*
         * update("Database.System", "SQLite"); if
         * (config.getString("Database.System").equalsIgnoreCase("SQLite")) {
         * dbType = "SQLite"; update("Database.File", "database.db");
         * set("Database.Host", null); set("Database.Port", null);
         * set("Database.Username", null); set("Database.Password", null);
         * set("Database.TablePrefix", null); set("Database.Name", null); }else
         * if (config.getString("Database.System").equalsIgnoreCase("MySQL")) {
         * dbType = "MySQL"; update("Database.Host", "localhost");
         * update("Database.Port", 3306); update("Database.Name", "minecraft");
         * update("Database.Username", "root"); update("Database.Password", "");
         * set("Database.File" , null); }else if
         * (config.getString("Database.System").equalsIgnoreCase("H2")) { dbType
         * = "H2"; update("Database.Host", "localhost"); update("Database.Name",
         * "minecraft"); update("Database.Username", "root");
         * update("Database.Password", ""); set("Database.File" , null);
         * set("Database.Port", null);
		}
         */
        config.save(configFile);
    }

    /**
     *
     * Return whether Chadmin is in Debug Mode or not
     *
     * @return The Debug Mode
     *
     */
    public boolean getDebug() {
        return config.getBoolean("General.Debug", false);
    }

    public String getDatabaseType() {
        return dbType;
    }

    public String getString(String path) {
        return config.getString(path);
    }

    public String getString(String path, String def) {
        return config.getString(path, def);
    }

    public boolean getBoolean(String path) {
        return config.getBoolean(path);
    }

    public boolean getBoolean(String path, Boolean def) {
        return config.getBoolean(path, def);
    }

    public int getInt(String path) {
        return config.getInt(path);
    }

    public int getInt(String path, int def) {
        return config.getInt(path, def);
    }

    public List<?> getList(String path) {
        return config.getList(path);
    }

    public List<?> getList(String path, List<?> def) {
        return config.getList(path, def);
    }

    public List<Integer> getIntegerList(String path) {
        return config.getIntegerList(path);
    }

    public Object get(String path) {
        return config.get(path);
    }

    public Object get(String path, Object def) {
        return config.get(path, def);
    }

    public boolean update(String path, Object value) {
        if (!config.contains(path)) {
            config.set(path, value);
            return false;
        } else {
            return true;
        }
    }

    public void set(String path, Object value) {
        config.set(path, value);
    }

    public boolean contains(String path) {
        return config.contains(path);
    }

    public String getGUID() {
        return getString("General.Statistics.GUID");
    }

    public void reload() {
        try {
            config.load(configFile);
        } catch (FileNotFoundException e) {
            if (getDebug()) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            if (getDebug()) {
                e.printStackTrace();
            }
        } catch (InvalidConfigurationException e) {
            if (getDebug()) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        try {
            config.save(configFile);
        } catch (IOException e) {
            if (getDebug()) {
                e.printStackTrace();
            }
        }
    }

    public void addDefault(String path, Object value) {
        config.addDefault(path, value);
    }

    public void addAlias(String cmd) {
        update("General.Aliases." + cmd.substring(0, 1).toUpperCase() + cmd.substring(1), false);
    }

    public boolean getAlias(String cmd) {
        return getBoolean("General.Aliases." + cmd.substring(0, 1).toUpperCase() + cmd.substring(1));
    }
}
