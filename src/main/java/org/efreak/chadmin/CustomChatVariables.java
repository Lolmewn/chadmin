package org.efreak.chadmin;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.economy.Economy;

public class CustomChatVariables {

	static Economy economy = null;
	static Plugin instance;
	
	public void initialize() {
		instance = Chadmin.getInstance();
		//Hook into Vault
        RegisteredServiceProvider<Economy> economyProvider = instance.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
	}
	
	public static String format(String message, ChatPlayer player) {
		if (economy != null) message = message.replaceAll("%money%", economy.format(economy.getBalance(player.getName())));
		return message;
	}
	
}
