package org.efreak.chadmin;

import java.io.File;
import java.io.IOException;

public class Filter {

	private File filterFile;
	
	public Filter(File file) {
		filterFile = file;
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public File getFile() {
		return filterFile;
	}
	
	public String filter(String msg) {
		return msg;
	}

}
