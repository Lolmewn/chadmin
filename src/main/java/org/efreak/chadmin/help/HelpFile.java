package org.efreak.chadmin.help;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.Configuration;

public class HelpFile {

	private static Configuration config;
	private File helpFile;
	private YamlConfiguration help;
	
	public void initialize() {
		config = new Configuration();
		helpFile = new File(Chadmin.getInstance().getDataFolder(), "help.yml");
		help = new YamlConfiguration();
		try {
			if (!helpFile.exists()) helpFile.createNewFile();
			help.load(helpFile);
			addContent();
			help.save(helpFile);
		} catch (FileNotFoundException e) {
			if (config.getDebug()) e.printStackTrace();
		} catch (IOException e) {
			if (config.getDebug()) e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			if (config.getDebug()) e.printStackTrace();
		}
	}
	
	private void addContent() {
		update("Help", "Helps you:D");
		update("Mute", "Mutes player");
		update("Channel.Create", "Create new Channels");
		update("Channel.List", "Lists all Channels");
		update("Channel.Set", "Set the Channel of a Player");
		update("Channel.Get", "Get the Channel of a Player");
		update("Channel.Remove", "Removes a Channel");
	}
	
	public String getHelp(String key) {
		return help.getString(key);
	}
	
	public boolean update(String path, Object value) {
		if (!help.contains(path)) {
			help.set(path, value);
			return false;
		}else return true;
	}
}
