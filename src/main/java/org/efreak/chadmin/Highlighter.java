package org.efreak.chadmin;

import org.bukkit.Bukkit;

public class Highlighter {

	private static Configuration config;
	
	static {
		config = Chadmin.getConfiguration();
	}
	
	public static String highlight(String msg) {
		System.out.println("Highlighting message: " + msg);
		String message = "";
		for (String msgPart : msg.split(" ")) {
			System.out.println(msgPart);
			if (Bukkit.getServer().getOfflinePlayer(msgPart).hasPlayedBefore()) {
				System.out.println(msgPart + " is a player");
				msgPart = config.getString("Highlighter.Format").replaceAll("%player%", msgPart);
			}
			message += msgPart + " ";
		}
		message = message.substring(0, message.length() - 1);
		return message;
	}
	
}
