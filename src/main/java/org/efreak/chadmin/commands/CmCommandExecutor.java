package org.efreak.chadmin.commands;

import java.util.HashMap;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import org.efreak.chadmin.IOManager;
import org.efreak.chadmin.commands.channel.*;
import org.efreak.chadmin.commands.general.*;

public class CmCommandExecutor implements CommandExecutor {
	
	private static IOManager io;
	private static HashMap<String, Command> commands;
	private static HashMap<String, Alias> aliases;
	
	public CmCommandExecutor() {
		io = new IOManager();
		commands = new HashMap<String, Command>();
		aliases = new HashMap<String, Alias>();
		io.sendConsole(io.translate("Plugin.LoadingCommands"));
		//Generalcommands
		registerCommand(new HelpCmd());
        registerCommand(new MuteCmd());
        registerCommand(new UnmuteCmd());
		//Channelcommands
		registerAlias("channel", new ChannelCommand());
		registerCommand(new ChannelCreateCmd());
		registerCommand(new ChannelListCmd());
		registerCommand(new ChannelSetCmd());
		registerCommand(new ChannelGetCmd());
		registerCommand(new ChannelRemoveCmd());
		io.sendConsole(io.translate("Plugin.CommandsLoaded"));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String commandLabel, String[] args) {
		if (args.length == 0) return false;
		else if (commands.containsKey("general." + args[0])) return commands.get("general." + args[0]).execute(sender, args, 1);
        else if(args.length == 1) return false;
		else if (commands.containsKey(args[0] + "." + args[1])) return commands.get(args[0] + "." + args[1]).execute(sender, args, 1);
		else return false;
	}
	
	public static void registerCommand(Command command) {
		commands.put(command.getCategory().toString().toLowerCase() + "." + command.getLabel(), command);
		Alias alias = aliases.get(command.getCategory().toString().toLowerCase());
		if (alias != null) alias.registerCommand(command);
	}
	
	public static void registerAlias(String category, Alias alias) {
		aliases.put(category, alias);
	}
}
