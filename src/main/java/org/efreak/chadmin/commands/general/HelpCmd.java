package org.efreak.chadmin.commands.general;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;
import org.efreak.chadmin.help.*;

public class HelpCmd extends Command {
		
	public HelpCmd() {
		super("help", "Help", "cm.help", new ArrayList<String>(), CommandCategory.GENERAL);
	}
	
	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		List<HelpTopic> helpTopics = HelpManager.getTopics();
		List<String> topics = new ArrayList<String>();
		for (int i = 0; i < helpTopics.size(); i++) {
			if (helpTopics.get(i).hasPerm(sender)) topics.add(helpTopics.get(i).format());
		}
		int pages = 1;
		if (topics.size() > 9) pages = (int)(((topics.size()) / 9F)+0.4F);
		if (args.length == 1) {
			io.send(sender, "&e---------------&f CHATMANAGER HELP(1/" + pages + ") &e---------------", false);
			for (int i = 0; i < 9 && i < topics.size(); i++) io.send(sender, topics.get(i), false);
		}else if (args.length == 2) {
			if (args[1].equalsIgnoreCase("caption")) {
	       		io.send(sender, "&e--------------&f CHATMANAGER HELP LEGEND &e--------------", false);
	       		io.send(sender, "&c#&f : Number", false);
	       		io.send(sender, "&cvalue&f : place holder", false);
	       		io.send(sender, "&evalue&f : defined value", false);
	       		io.send(sender, "&c[]&f : optional value", false);
	       		io.send(sender, "&c()&f : required value", false);
	       		io.send(sender, "&c|&f : (or) seperator", false);
			}else if (new Integer(args[1]) <= pages) {
				int page = new Integer(args[1]);
				io.send(sender, "&e---------------&f CHATMANAGER HELP(" + args[1] + "/" + pages + ") &e---------------", false);
				for (int i = (9*page-9); i < (9*page) && i < (topics.size()-1); i++) io.send(sender, topics.get(i), false);
			}
		}
		return true;
	}
}
