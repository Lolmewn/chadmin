/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package org.efreak.chadmin.commands.general;

import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.command.CommandSender;
import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class MuteCmd extends Command {

    public MuteCmd() {
        super("mute", "Mute", "cm.mute", Arrays.asList("(player)", "[channel]"), CommandCategory.GENERAL);
    }

    @Override
    public boolean execute(CommandSender sender, String[] args, Integer length) { //args[0] = mute
        if (args.length == 1) {
            sender.sendMessage(io.translate("Command.Mute.Usage"));
            return true;
        }
        String player = args[1];
        ChatPlayer cPlayer = Chadmin.getChatPlayer(player);
        if (args.length == 2) {
            cPlayer.getChannel().setMuted(cPlayer);
            if (Chadmin.getConfiguration().getBoolean("Channel.notifyMuted", true)) {
                cPlayer.getChannel().notify(io.translate("Command.Mute.Muted").replaceAll("%player%", cPlayer.getName()).replaceAll("%channel%", cPlayer.getChannel().getName()));
            }
            return true;
        }
        String channel = args[2];
        Channel ch = Chadmin.getChannel(channel);
        ch.setMuted(cPlayer);
        if (Chadmin.getConfiguration().getBoolean("Channel.notifyMuted", true)) {
            ch.notify(io.translate("Command.Mute.Muted").replaceAll("%player%", cPlayer.getName()).replaceAll("%channel%", cPlayer.getChannel().getName()));
        }
        return true;
    }
}
