package org.efreak.chadmin.commands.channel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.channel.ChannelMode;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

public class ChannelListCmd extends Command {

	public ChannelListCmd() {
		super("list", "Channel.List", "cm.channel.list", Arrays.asList("[#]"), CommandCategory.CHANNEL);
	}

	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		if (args.length < (1 + length)) io.sendFewArgs(sender, "/cm channel list [#]");
		else if (args.length > (2 + length)) io.sendManyArgs(sender, "/cm channel list [#]");
		else {
			if (has(sender, "channel.list")) {
				List<Channel> channels = Chadmin.getChannels();
				if (channels.size() != 0) {
					List<Channel> channelList = new ArrayList<Channel>();
					for (int i = 0; i < channels.size(); i++) {
						if (!channels.get(i).getMode().equals(ChannelMode.PRIVATE)) channelList.add(channels.get(i));
					}
					channels = channelList;
					int pages = (int)(((channels.size()) / 9F)+0.4F);
					if (args.length == (1 + length)) {
						io.send(sender, "&e-----------------&f CHANNELLIST (1/" + pages + ") &e-----------------", false);
						for (int i = 0; i < 9 && i < channels.size(); i++) {
							Channel channel = channels.get(i);
							io.send(sender, config.getString("Channel.List.Entry").replaceAll("%mode%", channel.getMode().toString()).replaceAll("%availability%", channel.getAvailability().toString()).replaceAll("%name%", channel.getName()).replaceAll("%players%", String.valueOf(channel.getOnlinePlayers().size()) + " Player(s) online"), false);
						}
					}else {
						int page = Integer.getInteger(args[1 + length]);
						if (page > pages) io.sendError(sender, io.translate("Command.PageDoesntExist"));
						else {
							io.send(sender, "&e-----------------&f CHANNELLIST (" + args[1 + length] + "/" + pages + ") &e-----------------", false);
							for (int i = 0; i < 9 && i < channels.size(); i++) {
								Channel channel = channels.get(i);
								io.send(sender, config.getString("Channel.List.Entry").replaceAll("%mode%", channel.getMode().toString()).replaceAll("%availability%", channel.getAvailability().toString()).replaceAll("%name%", channel.getName()).replaceAll("%players%", String.valueOf(channel.getOnlinePlayers().size())), false);
							}
						}
					}
				}else io.sendError(sender, "No Channels found!");
			}
		}
		return true;
	}	
}
