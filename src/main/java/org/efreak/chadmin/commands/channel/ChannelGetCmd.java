package org.efreak.chadmin.commands.channel;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

public class ChannelGetCmd extends Command {

	public ChannelGetCmd() {
		super("get", "Channel.Get", "channel.get", Arrays.asList("[player]"), CommandCategory.CHANNEL);
	}

	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		if (args.length < (1 + length)) io.sendFewArgs(sender, "/cm channel get [player]");
		else if (args.length > (2 + length)) io.sendManyArgs(sender, "/cm channel get [player]");
		else {
			if (args.length == (1 + length) && sender instanceof Player) {
				if (has(sender, "channel.get")) {
					ChatPlayer player = Chadmin.getChatPlayer(sender.getName());
					io.send(sender, io.translate("Command.Channel.Get.Your").replaceAll("%channel%", player.getChannel().getName()));
				}
			}else if (args.length == (2 + length)) {
				if (has(sender, "channel.set.other")) {
					ChatPlayer player = Chadmin.getChatPlayer(Bukkit.getOfflinePlayer(args[1 + length]).getName());
					io.send(sender, io.translate("Command.Channel.Get.Other").replaceAll("%player%", player.getName()).replaceAll("%channel%", player.getChannel().getName()));
				}
			}else if (args.length == (1 + length)) io.sendError(sender, io.translate("Command.Player.SpecifyPlayer"));
		}
		return true;
	}
	
}
