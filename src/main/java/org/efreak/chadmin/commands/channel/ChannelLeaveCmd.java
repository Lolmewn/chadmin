package org.efreak.chadmin.commands.channel;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

public class ChannelLeaveCmd extends Command {

	public ChannelLeaveCmd() {
		super("leave", "Channel.Leave", "channel.leave", Arrays.asList("(channel)", "[player]"), CommandCategory.CHANNEL);
	}

	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		if (args.length < (2 + length)) io.sendFewArgs(sender, "/cm channel set (channel) [player]");
		else if (args.length > (3 + length)) io.sendManyArgs(sender, "/cm channel set (channel) [player]");
		else {
			if (args.length == (2 + length) && sender instanceof Player) {
				if (has(sender, "channel.set")) {
					ChatPlayer player = Chadmin.getChatPlayer(sender.getName());
					Channel channel = Chadmin.getChannel(args[1 + length]);
					if (channel != null && player != null) {
						player.setChannel(channel);
						io.send(sender, io.translate("Command.Channel.Set.Your").replaceAll("%channel%", channel.getName()));
					}
					else io.sendError(sender, io.translate("Command.Channel.Set.UnknownChannel"));
				}
			}else if (args.length == (3 + length)) {
				if (has(sender, "channel.set.other")) {
					ChatPlayer player = Chadmin.getChatPlayer(Bukkit.getOfflinePlayer(args[2 + length]).getName());
					Channel channel = Chadmin.getChannel(args[1 + length]);
					if (channel != null && player != null) {
						player.setChannel(channel);
						io.send(sender, io.translate("Command.Channel.Set.Other").replaceAll("%player%", player.getName()).replaceAll("%channel%", channel.getName()));
					}
					else io.sendError(sender, io.translate("Command.Channel.Set.UnknownChannel"));
				}
			}else if (args.length == (2 + length)) io.sendError(sender, io.translate("Command.Player.SpecifyPlayer"));
		}
		return true;
	}
	
}
