package org.efreak.chadmin.commands.channel;

import org.efreak.chadmin.commands.Alias;

public class ChannelCommand extends Alias {

	public ChannelCommand() {
		super("channel", "All functions which are concerned with Channels from Chadmin");
	}

}
