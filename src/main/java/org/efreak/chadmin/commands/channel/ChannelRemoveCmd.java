package org.efreak.chadmin.commands.channel;

import java.util.Arrays;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

public class ChannelRemoveCmd extends Command {

	public ChannelRemoveCmd() {
		super("remove", "Channel.Remove", "cm.channel.remove", Arrays.asList("(channelname)"), CommandCategory.CHANNEL);
	}

	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		if (args.length < (2 + length)) io.sendFewArgs(sender, "/cm channel remove (channelname)");
		else if (args.length > (2 + length)) io.sendManyArgs(sender, "/cm channel remove (channelname)");
		else {
			if (has(sender, "channel.remove")) {
				Channel channel = Chadmin.getChannel(args[1 + length]);
				if (channel.isModerator(Chadmin.getChatPlayer((Player) sender))) {
					Chadmin.removeChannel(channel);
				}else io.sendError(sender, io.translate("Channel.NoModerator"));
			}
		}
		return true;
	}
}
