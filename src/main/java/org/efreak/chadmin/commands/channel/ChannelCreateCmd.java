package org.efreak.chadmin.commands.channel;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.channel.Channel;
import org.efreak.chadmin.channel.ChannelAvailability;
import org.efreak.chadmin.channel.ChannelMode;
import org.efreak.chadmin.channel.ChannelPassword;
import org.efreak.chadmin.commands.Command;
import org.efreak.chadmin.commands.CommandCategory;

public class ChannelCreateCmd extends Command {

	public ChannelCreateCmd() {
		super("create", "Channel.Create", "cm.channel.create", Arrays.asList("(channelname)", "[args]"), CommandCategory.CHANNEL);
	}

	@Override
	public boolean execute(CommandSender sender, String[] args, Integer length) {
		if (args.length < (2 + length)) io.sendFewArgs(sender, "/cm channel create (channelname) [args]");
		else {
			if (has(sender, "channel.create")) {
				List<String> argList = Arrays.asList(args);
				ChannelMode mode;
				ChannelAvailability availability;
				ChannelPassword password = null;
				Integer radius = null;
				//World world = null;
				Channel channel;
				try {
					//Get the channelmode
					if (argList.contains("-m")) mode = ChannelMode.valueOf(argList.get(argList.indexOf("-m") + 1).toUpperCase());
					else if (argList.contains("--mode")) mode = ChannelMode.valueOf(argList.get(argList.indexOf("--mode") + 1).toUpperCase());
					else mode = ChannelMode.PUBLIC;
				}catch (IllegalArgumentException e) {
					io.sendError(sender, io.translate("Command.Channel.Create.ModeDoesntExists"));
					if (config.getDebug()) e.printStackTrace();
					return true;
				}
				//get the channelavailability
				try {
					if (argList.contains("-a")) availability = ChannelAvailability.valueOf(argList.get(argList.indexOf("-a") + 1).toUpperCase());
					else if (argList.contains("--availability")) availability = ChannelAvailability.valueOf(argList.get(argList.indexOf("--availability") + 1));
					else availability = ChannelAvailability.GLOBAL;
				}catch (IllegalArgumentException e) {
					io.sendError(sender, io.translate("Command.Channel.Create.ModeDoesntExists"));
					if (config.getDebug()) e.printStackTrace();
					return true;
				}
				//get availabilty args
				if (availability.equals(ChannelAvailability.LOCAL)) {
					if (argList.contains("-r")) radius = Integer.getInteger(argList.get(argList.indexOf("-r") + 1));
					else if (argList.contains("--radius")) radius = Integer.getInteger(argList.get(argList.indexOf("-r") + 1));
					else {
						io.sendError(sender, io.translate("Command.Channel.Create.NoRadius"));
						return true;
					}
				}/*else if (availability.equals(ChannelAvailability.WORLD)) {
					if (argList.contains("-w")) world = Bukkit.getWorld(argList.get(argList.indexOf("-w") + 1));
					else if (argList.contains("--world")) world = Bukkit.getWorld(argList.get(argList.indexOf("--world") + 1));
					else {
						io.sendError(sender, io.translate("Command.Channel.Create.NoWorld"));
						return true;
					}
				}*/
				//get password
				if (argList.contains("-p")) password = new ChannelPassword(argList.get(argList.indexOf("-p") + 1));
				else if (argList.contains("--password")) password = new ChannelPassword(argList.get(argList.indexOf("--password") + 1));
				//create the channel
				if (availability.equals(ChannelAvailability.LOCAL)) {
					if (password != null) channel = new Channel(args[1 + length], mode, availability, radius, password);
					else channel = new Channel(args[1 + length], mode, availability, radius);
				}/*else if (availability.equals(ChannelAvailability.WORLD)) {
					if (password != null) channel = new Channel(args[1 + length], mode, availability, world, password);
					else channel = new Channel(args[1 + length], mode, availability, world);
				}*/else {
					if (password != null) channel = new Channel(args[1 + length], mode, availability, password);
					else channel = new Channel(args[1 + length], mode, availability);
				}
				//register the channel
				Chadmin.registerChannel(channel);
				channel.addModerator(Chadmin.getChatPlayer((Player) sender));
				io.send(sender, "Channel " + channel.getName() + " created!");
			}
		}
		return true;
	}
}
