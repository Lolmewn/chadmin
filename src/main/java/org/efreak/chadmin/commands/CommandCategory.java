package org.efreak.chadmin.commands;

public enum CommandCategory {

	/**
	 * 
	 * All commands, which are concerned with Chadmin
	 * Layout: /cm (label) (args)
	 * 
	 */
	
	GENERAL,
	
	/**
	 * 	 
	 * All commands, which are concerned with Channels
	 * Layout: /cm channel (label) (args) or /channel (label) (args)
	 * 
	 */
	
	CHANNEL
}
