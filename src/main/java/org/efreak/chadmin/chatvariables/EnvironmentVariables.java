package org.efreak.chadmin.chatvariables;

import com.onarandombox.MultiverseCore.api.Core;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.efreak.chadmin.ChatPlayer;

public class EnvironmentVariables implements ChatVariableHandler{
    
    Core core;

	@ChatVariable(name = "%world%")
	public String world(ChatPlayer player) {
		return player.getPlayer().getWorld().getName();
	}
    
    @ChatVariable(name="%mv_world%")
	public String getMultiVerseWorld(ChatPlayer player){
        if(core == null){
            Plugin p = Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            if(p == null){
                return world(player);
            }
            core = (Core)p;
        }
        return core.getMVWorldManager().getMVWorld(player.getPlayer().getWorld()).getAlias();        
    }
    
	@ChatVariable(name = "%x%")
	public String posX(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLocation().getBlockX());
	}
	
	@ChatVariable(name = "%y%")
	public String posY(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLocation().getBlockY());
	}
	
	@ChatVariable(name = "%z%")
	public String posZ(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLocation().getBlockZ());
	}
	
	@ChatVariable(name = "%chunkx%")
	public String chunkX(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLocation().getChunk().getX());
	}

	@ChatVariable(name = "%chunkz%")
	public String chunkZ(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLocation().getChunk().getZ());
	}
}
