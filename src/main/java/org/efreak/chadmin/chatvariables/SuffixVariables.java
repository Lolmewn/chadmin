package org.efreak.chadmin.chatvariables;

import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.Permissions;

public class SuffixVariables implements ChatVariableHandler{

	@ChatVariable(name = "%group_suffix%")
	public String groupSuffix(ChatPlayer player) {
		return Permissions.getGroupSuffix(player.getPlayer());
	}
	
	@ChatVariable(name = "%suffix%")
	public String suffix(ChatPlayer player) {
		return Permissions.getSuffix(player.getPlayer());
	}
	
}
