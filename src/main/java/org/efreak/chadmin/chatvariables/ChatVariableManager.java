package org.efreak.chadmin.chatvariables;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.efreak.chadmin.ChatPlayer;

public class ChatVariableManager {
	
	private Map<String, Method> methods = new HashMap<String, Method>();
	
	public String parseVariables(String msg, ChatPlayer player) {
		Object[] keys = methods.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			if (msg.contains(keys[i].toString())) msg = msg.replaceAll(keys[i].toString(), invoke(keys[i].toString(), player));
		}
		return msg;
	}
	
	private String invoke(String key, ChatPlayer player) {
		try {
			ChatVariableHandler handler = (ChatVariableHandler) methods.get(key).getDeclaringClass().newInstance();
			return methods.get(key).invoke(handler, player).toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public void registerHandler(final Class<?> class_) {
		for (final Method method : class_.getMethods()) {
			if (!method.isAnnotationPresent(ChatVariable.class)) continue;
			final ChatVariable variable = method.getAnnotation(ChatVariable.class);
			methods.put(variable.name(), method);
		}
	}
}