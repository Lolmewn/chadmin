package org.efreak.chadmin.chatvariables;

import org.efreak.chadmin.ChatPlayer;

public class PlayerVariables implements ChatVariableHandler{

	@ChatVariable(name = "%name%")
	public String name(ChatPlayer player) {
		return player.getName();
	}
	
	@ChatVariable(name = "%displayname%")
	public String displayname(ChatPlayer player) {
		return player.getPlayer().getDisplayName();
	}
	
	@ChatVariable(name = "%listname%")
	public String listname(ChatPlayer player) {
		return player.getPlayer().getPlayerListName();
	}
	
	@ChatVariable(name = "%level%")
	public String level(ChatPlayer player) {
		return String.valueOf(player.getPlayer().getLevel());
	}
	
	@ChatVariable(name = "%gamemode%")
	public String gamemode(ChatPlayer player) {
		return player.getPlayer().getGameMode().toString();
	}
	
	 @ChatVariable(name = "%channel%")
	 public String channel(ChatPlayer player) {
		 return player.getChannel().getName();
	 }
	 
	 @ChatVariable(name = "%health%")
	 public String health(ChatPlayer player) {
		 return String.valueOf(player.getPlayer().getHealth());
	 }
	
	 @ChatVariable(name = "%foodlevel%")
	 public String foodlevel(ChatPlayer player) {
		 return String.valueOf(player.getPlayer().getFoodLevel());
	 }
}
