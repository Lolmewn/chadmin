package org.efreak.chadmin.chatvariables;

import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.Permissions;

public class PrefixVariables implements ChatVariableHandler{

	@ChatVariable(name = "%group_prefix%")
	public String groupPrefix(ChatPlayer player) {
		return Permissions.getGroupPrefix(player.getPlayer());
	}
	
	@ChatVariable(name = "%prefix%")
	public String prefix(ChatPlayer player) {
		return Permissions.getPrefix(player.getPlayer());
	}
	
}
