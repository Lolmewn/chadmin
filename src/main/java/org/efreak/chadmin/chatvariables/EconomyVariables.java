package org.efreak.chadmin.chatvariables;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.efreak.chadmin.ChatPlayer;

public class EconomyVariables implements ChatVariableHandler{

	static Economy economy;
	
	static {
		RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) economy = economyProvider.getProvider();
	}

	@ChatVariable(name = "%money%")
	public String money(ChatPlayer player) {
		if (economy != null) return economy.format(economy.getBalance(player.getName()));
		else return "";
	}	
}
