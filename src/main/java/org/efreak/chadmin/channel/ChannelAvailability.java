package org.efreak.chadmin.channel;

public enum ChannelAvailability {

	/**
	 * 
	 * Everywhere available
	 * 
	 */
	GLOBAL,
	
	/**
	 * 
	 * Only on a certain World available
	 * 
	 */
	WORLD,
	
	/**
	 * 
	 * Only in a certain Radius available
	 * 
	 */
	LOCAL
	
}
