package org.efreak.chadmin.channel;

import java.util.logging.Logger;

public class ChannelLogger extends Logger{

	protected ChannelLogger(String name, ChannelLoggingHandler handler) {
		super(name, null);
		this.setUseParentHandlers(true);
		this.addHandler(handler);
	}
}
