package org.efreak.chadmin.channel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.ChatPlayer;
import org.efreak.chadmin.Configuration;
import org.efreak.chadmin.Database;
import org.efreak.chadmin.Filter;
import org.efreak.chadmin.Highlighter;
import org.efreak.chadmin.IOManager;

public class Channel {

    private static Configuration config;
    private static File channelDir;
    private static IOManager io;
    private File channelFile;
    private YamlConfiguration channelConfig = null;
    private String name = "";
    private ChannelMode mode = null;
    private ChannelPassword password = null;
    private String permission = null;
    private ChannelAvailability availability = null;
    private double radius;
    //private World world = null;
    private String format = "default";
    private List<OfflinePlayer> players = null;
    private List<ChatPlayer> onlinePlayers = null;
    private List<OfflinePlayer> moderators = null;
    private List<OfflinePlayer> mutes = null;
    private List<OfflinePlayer> bans = null;
    private Filter filter = null;
    private boolean isFilterEnabled = false;
    private boolean consoleOutput = true;
    private ChannelLogger logger = null;
    private ChannelLoggingHandler logHandler = null;
    private File logFile = null;
    private boolean isLoggingEnabled = true;

    public Channel(String arg1Name) {
        name = arg1Name;
        init();
        load();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, ChannelPassword arg4Password) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        password = arg4Password;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, int arg4Radius) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        radius = arg4Radius;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, int arg4Radius, ChannelPassword arg5Password) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        radius = arg4Radius;
        password = arg5Password;
        save();
    }

    /*
     * public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability
     * arg3Availability, World arg4World) { name = arg1Name; init(); mode =
     * arg2Mode; availability = arg3Availability; world = arg4World; save();
	}
     */
    /*
     * public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability
     * arg3Availability, World arg4World, ChannelPassword arg5Password) { name =
     * arg1Name; init(); mode = arg2Mode; availability = arg3Availability; world
     * = arg4World; password = arg5Password; save();		
	}
     */
    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, String arg4Permissions) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        permission = arg4Permissions;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, String arg4Permissions, ChannelPassword arg5Password) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        permission = arg4Permissions;
        password = arg5Password;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, String arg4Permissions, int arg5Radius) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        permission = arg4Permissions;
        radius = arg5Radius;
        save();
    }

    public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability arg3Availability, String arg4Permissions, int arg5Radius, ChannelPassword arg6Password) {
        name = arg1Name;
        init();
        mode = arg2Mode;
        availability = arg3Availability;
        permission = arg4Permissions;
        radius = arg5Radius;
        password = arg6Password;
        save();
    }

    /*
     * public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability
     * arg3Availability, String arg4Permissions, World arg5World) { name =
     * arg1Name; init(); mode = arg2Mode; availability = arg3Availability;
     * permission = arg4Permissions; world = arg5World; save();		
	}
     */
    /*
     * public Channel(String arg1Name, ChannelMode arg2Mode, ChannelAvailability
     * arg3Availability, String arg4Permissions, World arg5World,
     * ChannelPassword arg6Password) { name = arg1Name; init(); mode = arg2Mode;
     * availability = arg3Availability; permission = arg4Permissions; world =
     * arg5World; password = arg6Password; save();			
	}
     */
    private void load() {
        try {
            channelConfig.load(channelFile);
            mode = ChannelMode.valueOf(channelConfig.getString("Channelsettings.Mode"));
            availability = ChannelAvailability.valueOf(channelConfig.getString("Channelsettings.Availability"));
            format = channelConfig.getString("Channelsettings.Format");
            consoleOutput = channelConfig.getBoolean("Channelsettings.ConsoleOutput");
            isFilterEnabled = channelConfig.getBoolean("Filter.Enabled");
            filter = new Filter(new File(channelDir, channelConfig.getString("Filter.File")));
            isLoggingEnabled = channelConfig.getBoolean("Logging.Enabled");
            logFile = new File(channelDir, channelConfig.getString("Logging.File"));
            logHandler.setupFile();
            if (channelConfig.get("Channelsettings.Radius") != null) {
                radius = channelConfig.getInt("Channelsettings.Radius");
            }
            //if (channelConfig.get("Channelsettings.World") != null) world = Chadmin.getInstance().getServer().getWorld(channelConfig.getString("Channelsettings.World"));
            if (channelConfig.get("Channelsettings.Password") != null) {
                password = new ChannelPassword(channelConfig.getString("Channelsetting.Password"));
            }
            if (channelConfig.get("Channelsettings.Permissions") != null) {
                permission = channelConfig.getString("Channelsettings.Permissions");
            }
            if (channelConfig.get("Players") != null) {
                players = parseStringList(channelConfig.getStringList("Players"));
            }
            if (channelConfig.get("Moderators") != null) {
                moderators = parseStringList(channelConfig.getStringList("Moderators"));
            }
            if (channelConfig.get("Mutes") != null) {
                mutes = parseStringList(channelConfig.getStringList("Mutes"));
            }
            if (channelConfig.get("Bans") != null) {
                bans = parseStringList(channelConfig.getStringList("Bans"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        if (filter == null) {
            filter = new Filter(new File(channelDir, name.replaceAll(" ", "_") + "_filter.txt"));
        }
        if (logFile == null) {
            logFile = new File(channelDir, name.replaceAll(" ", "_") + ".log");
            logHandler.setupFile();
        }
        channelConfig.set("Channelsettings.Mode", mode.toString());
        channelConfig.set("Channelsettings.Availability", availability.toString());
        if (radius != 0) {
            channelConfig.set("Channelsettings.Radius", radius);
        }
        //if (world == null) channelConfig.set("Channelsettings.World", null);
        //else channelConfig.set("Channelsettings.World", world.getName());
        if (password == null) {
            channelConfig.set("Channelsettings.Password", null);
        } else {
            channelConfig.set("Channelsettings.Password", password.getPassword());
        }
        channelConfig.set("Channelsettings.Permissions", permission);
        channelConfig.set("Channelsettings.Format", format);
        channelConfig.set("Channelsettings.ConsoleOutput", consoleOutput);
        channelConfig.set("Logging.Enabled", isLoggingEnabled);
        channelConfig.set("Logging.File", logFile.getName());
        channelConfig.set("Filter.Enabled", isFilterEnabled);
        channelConfig.set("Filter.File", filter.getFile().getName());
        channelConfig.set("Players", parsePlayerList(players));
        channelConfig.set("Moderators", parsePlayerList(moderators));
        channelConfig.set("Mutes", parsePlayerList(mutes));
        channelConfig.set("Bans", parsePlayerList(bans));
        try {
            channelConfig.save(new File(channelDir, name.replaceAll(" ", "_") + ".yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        config = new Configuration();
        io = new IOManager();
        channelDir = new File(Chadmin.getInstance().getDataFolder(), "channels");
        channelFile = new File(channelDir, name.replaceAll(" ", "_") + ".yml");
        onlinePlayers = new ArrayList<ChatPlayer>();
        players = new ArrayList<OfflinePlayer>();
        moderators = new ArrayList<OfflinePlayer>();
        mutes = new ArrayList<OfflinePlayer>();
        bans = new ArrayList<OfflinePlayer>();
        format = "default";
        if (channelConfig == null) {
            channelConfig = new YamlConfiguration();
        }
        logHandler = new ChannelLoggingHandler(this);
        logger = new ChannelLogger(name, logHandler);
    }

    public String getName() {
        return name;
    }

    public void chat(AsyncPlayerChatEvent event) {
        if (mutes.contains(event.getPlayer())) {
            return;
        }
        if (isLoggingEnabled) {
            logger.info(event.getPlayer().getName() + " " + event.getMessage());
        }
        List<Player> sendingTo = new ArrayList<Player>();
        if (availability.equals(ChannelAvailability.GLOBAL)) {
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).isOnline()) {
                    ChatPlayer player = Chadmin.getChatPlayer(players.get(i).getName());
                    sendingTo.add(player.getPlayer());
                    //player.getPlayer().sendMessage(Chadmin.format(event.getMessage(), Chadmin.getChatPlayer(event.getPlayer())));
                }
            }
        } else if (availability.equals(ChannelAvailability.WORLD)) {
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).isOnline()) {
                    ChatPlayer player = Chadmin.getChatPlayer(players.get(i).getName());
                    if (player.getPlayer().getWorld().equals(event.getPlayer().getWorld())) {
                        sendingTo.add(player.getPlayer());
                        //player.getPlayer().sendMessage(Chadmin.format(event.getMessage(), Chadmin.getChatPlayer(event.getPlayer())));
                    }
                }
            }
        } else {
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).isOnline()) {
                    if (players.get(i).getPlayer().getLocation().distance(event.getPlayer().getLocation()) <= radius) {
                        ChatPlayer player = Chadmin.getChatPlayer(players.get(i).getName());
                        sendingTo.add(player.getPlayer());
                        //player.getPlayer().sendMessage(Chadmin.format(event.getMessage(), Chadmin.getChatPlayer(event.getPlayer())));
                    }
                }
            }
        }
        event.getRecipients().clear();
        event.getRecipients().addAll(sendingTo);
        event.setFormat(Chadmin.getFormat(Chadmin.getChatPlayer(event.getPlayer())).replace(event.getPlayer().getName(), "%s").replace("%msg%", "%s"));
        if (config.getBoolean("Highlighter.Enabled")) event.setMessage(Highlighter.highlight(event.getMessage()));
        event.setMessage(config.getBoolean("Format.Colorcodes") ? io.parseColor(event.getMessage()) : event.getMessage());
    }

    public String getFormat() {
        if (format.equalsIgnoreCase("default")) {
            return config.getString("Format.Message");
        } else {
            return format;
        }
    }

    public ChannelMode getMode() {
        return mode;
    }

    public ChannelAvailability getAvailability() {
        return availability;
    }

    public List<OfflinePlayer> getPlayers() {
        return players;
    }

    public List<ChatPlayer> getOnlinePlayers() {
        return onlinePlayers;
    }

    public void addPlayer(ChatPlayer player) {
        if (!players.contains(player.getOfflinePlayer())) {
            players.add(player);
        }
        if (player.isOnline()) {
            onlinePlayers.add(player);
        }
    }

    public void removePlayer(ChatPlayer player) {
        players.remove(players.indexOf(player));
        onlinePlayers.remove(player);
    }

    private List<String> parsePlayerList(List<OfflinePlayer> original) {
        List<String> output = new ArrayList<String>();
        for (int i = 0; i < original.size(); i++) {
            output.add(original.get(i).getName());
        }
        return output;
    }

    private List<OfflinePlayer> parseStringList(List<String> original) {
        List<OfflinePlayer> output = new ArrayList<OfflinePlayer>();
        for (int i = 0; i < original.size(); i++) {
            output.add(Bukkit.getOfflinePlayer(original.get(i)));
        }
        return output;
    }

    public boolean isModerator(ChatPlayer player) {
        if (moderators.contains(player.getOfflinePlayer())) {
            return true;
        } else {
            return false;
        }
    }

    public void addModerator(ChatPlayer player) {
        moderators.add(player);
    }

    public void removeModerator(ChatPlayer player) {
        moderators.remove(player);
    }

    public void remove() {
        for (int i = 0; i < onlinePlayers.size(); i++) {
            ChatPlayer player = onlinePlayers.get(i);
            player.getPlayer().sendMessage(io.parseColor(io.translate("Channel.Removed").replaceAll("%channel%", Chadmin.getDefaultChannel().getName())));
            player.setChannel(Chadmin.getDefaultChannel());
        }
        channelConfig = null;
        channelFile.delete();
        filter.getFile().delete();
        logFile.delete();
    }

    public static File getChannelDir() {
        return channelDir;
    }

    public File getLoggingFile() {
        return logFile;
    }

    public void goOnline(ChatPlayer player) {
        onlinePlayers.add(player);
    }

    public void goOffline(ChatPlayer player) {
        onlinePlayers.remove(player);
    }
    
    public void setMuted(OfflinePlayer player){
        this.mutes.add(player);
    }
    
    public void unMute(OfflinePlayer player){
        this.mutes.remove(player);
    }
    
    public void notify(String message){
        for(ChatPlayer player : this.getOnlinePlayers()){
            player.getPlayer().sendMessage(message);
        }
    }
}
