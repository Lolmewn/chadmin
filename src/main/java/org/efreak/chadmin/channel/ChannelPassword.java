package org.efreak.chadmin.channel;

public class ChannelPassword {

	String password = "";
	
	public ChannelPassword(String arg1Password) {
		password = arg1Password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String arg1Password) {
		password = arg1Password;
	}
	
}
