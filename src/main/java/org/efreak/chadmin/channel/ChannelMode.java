package org.efreak.chadmin.channel;

public enum ChannelMode {

	/**
	 * 
	 * Unlisted Channel
	 * 
	 */
	PRIVATE,
	
	/**
	 * 
	 * Listed Channel
	 * 
	 */
	PUBLIC
	
}
