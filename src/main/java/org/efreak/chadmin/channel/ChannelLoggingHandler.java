package org.efreak.chadmin.channel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

import org.efreak.chadmin.Chadmin;
import org.efreak.chadmin.Configuration;
import org.efreak.chadmin.Database;
import org.efreak.chadmin.IOManager;

public class ChannelLoggingHandler extends Handler {
	
	public static Configuration config;
	public static Database db;
	public static IOManager io;
	public FileOutputStream fileOutputStream;
	public PrintWriter printWriter;
	public File file;
	public File dir;
	public Channel channel;

	public ChannelLoggingHandler(Channel arg1Channel) {
		super();
		config = new Configuration();
		db = Chadmin.getDb();
		io = Chadmin.getIOManager();
		channel = arg1Channel;
		setFormatter(new SimpleFormatter());
	}
	
	public void setupFile() {
		dir = Channel.getChannelDir().getAbsoluteFile();
		file = new File(dir, channel.getLoggingFile().getName());
		try {
			if (!file.exists()) file.createNewFile();
			fileOutputStream = new FileOutputStream(file);
			printWriter = new PrintWriter(fileOutputStream, true);
		} catch(Exception e){
			if (config.getDebug()) e.printStackTrace();
		}
	}
	
	public void publish(LogRecord arg1record) {
		if (!isLoggable(arg1record)) return;
		printWriter.println(this.getFormatter().format(arg1record));
	}

	public void flush() {
		printWriter.flush();
	}

	public void close() throws SecurityException {
		printWriter.close();
	}
		
	public void shutdown() {
		if (fileOutputStream != null) {
			try {
				fileOutputStream.flush();
				fileOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (printWriter != null) {
			printWriter.flush();
			printWriter.close();
		}
	}
}
