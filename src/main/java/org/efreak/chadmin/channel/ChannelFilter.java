package org.efreak.chadmin.channel;

import java.io.File;
import java.io.IOException;

public class ChannelFilter {

	private File filterFile;
	
	public ChannelFilter(File file) {
		filterFile = file;
		if (!file.exists())
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public File getFile() {
		return filterFile;
	}
	
	public String filter(String msg) {
		return msg;
	}

}
