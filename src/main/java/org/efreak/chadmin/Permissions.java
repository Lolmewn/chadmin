package org.efreak.chadmin;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Permissions {

    private static Permission vault = null;
    private static Chat vaultChat = null;
    private static IOManager io;
    private static Configuration config;
    public static boolean usePerms;
    private static boolean forceSuper;
    private static String permSystem;

    public void initialize() {
        io = new IOManager();
        config = new Configuration();
        usePerms = config.getBoolean("General.Use-Permissions");
        forceSuper = config.getBoolean("General.Force-SuperPerms");
        if (forceSuper) {
            io.sendConsole(io.translate("Permissions.ForceSuper"));
            return;
        }
        if (usePerms) {
            if (Bukkit.getServer().getPluginManager().getPlugin("Vault") != null) {
                if (config.getBoolean("General.Use-Vault")) {
                    RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
                    if (permissionProvider != null) {
                        vault = permissionProvider.getProvider();
                        permSystem = "Vault";
                        io.sendConsole(io.translate("Permissions.Found").replaceAll("%perms%", "Vault"));
                    }
                    RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
                    if (chatProvider != null) {
                        vaultChat = chatProvider.getProvider();
                    }
                }
                io.sendConsoleWarning(io.translate("Permissions.NoPerms"));
                io.sendConsole(io.translate("Permissions.OP"));
                usePerms = false;
            }
        } else {
            io.sendConsole(io.translate("Permissions.OP"));
        }
    }

    public void shutdown() {
    }

    public static boolean has(CommandSender sender, String perm) {
        return has(sender, perm, true);
    }

    public static boolean has(CommandSender sender, String perm, boolean log) {
        return sender.hasPermission(perm);
    }

    public static String getPrefix(Player player) {
        if(vaultChat != null){
            return vaultChat.getPlayerPrefix(player);
        } else {
            String prefix = "";

            return prefix;
        }
    }

    public static String getSuffix(Player player) {
        if(vaultChat != null){
            return vaultChat.getPlayerSuffix(player);
        } else {
            String suffix = "";

            return suffix;
        }
    }

    public static String getGroupSuffix(Player player) {
        if(vaultChat != null){
            return vaultChat.getGroupSuffix(player.getWorld(), vaultChat.getPrimaryGroup(player));
        } else {
            return getSuffix(player);
        }
    }

    public static String getGroupPrefix(Player player) {
        if(vaultChat != null){
            return vaultChat.getGroupPrefix(player.getWorld(), vaultChat.getPrimaryGroup(player));
        } else {
            return getPrefix(player);
        }
    }

    @Deprecated
    public static String getPlayerSuffix(Player player) {
        return getSuffix(player);
    }

    @Deprecated
    public static String getPlayerPrefix(Player player) {
        return getPrefix(player);
    }
}