package org.efreak.chadmin;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.efreak.chadmin.channel.Channel;

public class ChatListener implements Listener {

    @EventHandler()
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        ChatPlayer player = Chadmin.getChatPlayer(event.getPlayer().getName());
        if (player == null) {
            player = new ChatPlayer(event.getPlayer());
            Chadmin.addChatPlayer(player);
            Chadmin.getChatPlayer(event.getPlayer()).getChannel().goOnline(Chadmin.getChatPlayer(event.getPlayer()));
        }
        player.getChannel().chat(event);
        //event.setCancelled(true);
    }

    @EventHandler()
    public void onPlayerLogin(PlayerLoginEvent event) {
        Chadmin.addChatPlayer(new ChatPlayer(event.getPlayer()));
        Chadmin.getChatPlayer(event.getPlayer()).getChannel().goOnline(Chadmin.getChatPlayer(event.getPlayer()));
    }

    @EventHandler()
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        ChatPlayer chatPlayer = Chadmin.getChatPlayer(player);
        Channel playerChannel = chatPlayer.getChannel();
        Chadmin.removeChatPlayer(player.getName());
        playerChannel.goOffline(chatPlayer);
    }
}
