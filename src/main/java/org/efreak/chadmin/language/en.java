package org.efreak.chadmin.language;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.efreak.chadmin.Chadmin;


public class en extends Language {

	private static YamlConfiguration lang;
	private static File langFile;
	
	@Override
	public void createLanguageFile() {
		langFile = new File(Chadmin.getInstance().getDataFolder(), "lang" + File.separator + "en.lang");
		if (!langFile.exists()) {
			try {
				langFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateLanguage() {
		lang = new YamlConfiguration();
		try {
			lang.load(langFile);
			
			set("Plugin.Done", "&aDone!");
			set("Plugin.LoadingCommands", "&2Loading Commands...");
			set("Plugin.CommandsLoaded", "&aCommands successfully loaded!");
			
			set("AutoUpdater.NewVersion", "There is a new Version available: %name%");
			set("AutoUpdater.Running", "You are running %name%");
			set("AutoUpdater.UpToDate", "No updates found!");
			set("AutoUpdater.CheckingUpdates", "Checking for updates...");
			
			set("Channels.Loading", "&2Loading Channels...");
			set("Channels.Saving", "&2Saving Channels...");
			
			set("Channel.Found", "&2Found Channel %channel%");
			set("Channel.Removed", "The Channel you were in was removed. You are now in the Channel %channel%");
			set("Channel.NoModerator", "You aren't a Moderator of this Channnel");
			
			set("Permissions.ForceSuper", "&2Superperms forced!");
			set("Permissions.Found", "&a%perms% support enabled!");
			set("Permissions.NoPerms", "&eNo Permissions System Found!");
			set("Permissions.OP", "&2Using OP-Rights for Commands!");

			set("Downloader.Downloading", "&2Downloading %file% (%filesize%)");
			set("Downloader.Downloaded", "&aDownload of %file% finished.");
			set("Downloader.Error", "&cError Downloading File: %file%.");
			
			set("Thread.Start", "&a%thread% started! Interval is %interval% seconds");
			set("Thread.Stopping", "&2Stopping %thread%...");
			set("Thread.Stop", "&aSuccessfully stopped %thread%!");
			set("Thread.Error", "&cCould not stop %thread%");
			
			set("Command.FewArgs", "Too few Arguments");
			set("Command.ManyArgs", "Too many Arguments");
			set("Command.Usage", "Usage: %usage%");
			set("Command.NoPerm", "'&cYou dont have Permission to do that!");
			set("Command.Language.Get", "The Language is %lang%.");
			set("Command.Language.Set", "The Language was set to %lang%!");
			set("Command.Language.Error", "The Language %lang% doesn't exists!");
			set("Command.Player.UnknownPlayer", "&cUnknown Player!");
			set("Command.Player.SpecifyPlayer", "&cPlease specify a Player!");
			set("Command.Channel.Set.Your", "Your Channel was set to %channel%!");
			set("Command.Channel.Set.Other", "The Channel of %player% was set to %channel%!");
			set("Command.Channel.Set.UnknownChannel", "&cUnknown Channel");
			set("Command.Channel.Get.Your", "Your Channel is %channel%!");
			set("Command.Channel.Get.Other", "The Channel of %player% is %channel%!");
			set("Command.Channel.Removed", "The Channel %channel% was successfully removed!");
			set("Command.Channel.Create.NoRadius", "&cPlease specify a Radius with -r (radius)!");
			set("Command.Channel.Create.NoWorld", "&cPlease specify a World with -w (worldname)!");
            
            set("Command.Mute.Usage", "Correct usage: /ch mute (player) [channel]");
            set("Command.Mute.Muted", "Player %player% was muted from this channel!");
            
            set("Command.Unmute.Usage", "Correct usage: /ch unmute (player) [channel]");
            set("Command.Unmute.Unmuted", "Player %player% was unmuted from this channel!");
			lang.save(langFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String translate(String key) {
		return lang.getString(key);
	}

	@Override
	public File getFile() {
		return langFile;
	}

	@Override
	public YamlConfiguration getKeys() {
		return lang;
	}
	
	@Override
	public String getName() {
		return "en";
	}

	@Override
	public void set(String key, String value) {
		if (lang.get(key) == null) lang.set(key, value);
	}
	
}
